README.md

#Projeto João Garbini 

##Entrega: Introdução ao GitLab

Nesta entrega, você irá praticar o uso de instruções de linhas de comando para criar vários arquivos e diretórios enquanto usa o controle de versão git para registrar suas alterações.

Talvez este tutorial do gitlab, junto com os vídeos e leituras anteriores, seja útil.
Instruções

    Se você ainda não tiver feito isso nas Atividades anteriores, crie e configure sua conta no GitLab.
    Se você ainda não tiver feito isso nas Atividades anteriores, configure novas chaves SSH em seu computador, e adicione a chave pública a sua conta do GitLab.
    Crie um novo diretório em seu computador para este projeto chamado "day1" e inicialize-o como um repositório git.
    Crie um pequeno arquivo README.md. Use o git para adicionar e fazer o commit desse novo arquivo.
    Crie dois novos diretórios no seu projeto; nomeie-os "dir1" e "dir2".
    Dentro do dir1, crie um arquivo chamado "file1". Dentro do dir2 crie um arquivo chamado "file2".
    Use o git para fazer o add e o commit dos novos diretórios ao seu repositório local.
    Conecte seu repositório local a um repositório no GitLab e faça o push.
    Envie a URL do repositório GitLab abaixo. Ela deve se parecer com isso: "https://gitlab.com/seu_nomede usuário/day1"
